### STAGE 1: Build ###
FROM node:alpine AS build
WORKDIR /node
COPY *.json ./
RUN ls
RUN npm install > /dev/null 2>&1
ADD . .
ARG API_ROOT="https://ninja-fargate.test.coherentprojects.net"
RUN sed "s|https://conduit.productionready.io/api|${API_ROOT}|" -i src/agent.js && \ 
npm run build > /dev/null 2>&1

### STAGE 2: Run ###
FROM nginx AS run
WORKDIR /usr/share/nginx/html
ENV BUILD_DIR="/node/build/"
COPY --from=build --chown=nginx:nginx ${BUILD_DIR} ./
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]